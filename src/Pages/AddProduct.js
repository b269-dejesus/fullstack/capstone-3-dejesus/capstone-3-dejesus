import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';

import {Navigate, useNavigate} from 'react-router-dom';

import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Register() {

    const navigate = useNavigate();
    const {user, setUser} = useContext(UserContext);
    const {isAdmin } = useContext(UserContext);


    // to store values of the input fields
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    const [stock, setStock] = useState("");
    
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);
   

    useEffect(() => {
        if(name !== "" && description !== "" && price!=="" && stock !== "" ) {
            setIsActive(true);
        } else {
            setIsActive(false);

        }
    }, [name, description, price, stock]);

   
    // function to simulate user registration
    function addProduct(e) {
        // Prevents page from reloading       
        e.preventDefault();

        // Clear input fields
        setName("");
        setDescription("");
        setPrice("");
        setStock("");
       
        


        fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
               
                name: name,
                description: description,
                price: price,
                stock:stock
               
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data === true) {
                Swal.fire({
                    title: "Product added Successfully",
                    icon: "success",
                    text: "You have been Successfully registered."
                })

                navigate("/products")

            } else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                })
            }

        })
    };
  


    return (
        (!user.isAdmin)?
        <Navigate to ="/products/all" />
        :
        <Form onSubmit={(e) => addProduct(e)}>
            
             <Form.Group controlId="name">
                <Form.Label>Product Name</Form.Label>
                <Form.Control 
                    type="Text" 
                    placeholder="Enter a product" 
                    value={name}
                    onChange={e => setName(e.target.value)}
                    required
                />
                {/*<Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>*/}
            </Form.Group>

             <Form.Group controlId="description">
                <Form.Label>Product Description</Form.Label>
                <Form.Control 
                    type="Text" 
                    placeholder="Product description" 
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                    required
                />
                
            </Form.Group>


            <Form.Group controlId="price">
                <Form.Label>Product price</Form.Label>
                <Form.Control 
                    type="number" 
                    placeholder="Enter price" 
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                    required
                />
              
            </Form.Group>


            <Form.Group controlId="stock">
                <Form.Label>Product Quantity</Form.Label>
                <Form.Control 
                    type="number" 
                    placeholder="stock" 
                    value={stock}
                    onChange={e => setStock(e.target.value)}
                    required
                />
            </Form.Group>


            { isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
    )

};

