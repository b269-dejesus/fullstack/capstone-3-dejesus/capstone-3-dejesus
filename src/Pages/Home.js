import Banner from '../components/Banner';
import Products from './Products';


export default function Home() {

	const data = {
		title: "LAZHOPPY",
		content: "Shopping like there's no tommorrow",
		destination: "/products",
		label: "Shop na!"
	}


	return (
		<>
		<Banner data={data} />
    	<Products />
    	
		</>
	)
}



