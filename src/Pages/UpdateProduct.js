import { useState,useContext,useEffect } from "react";
import { Form, Button, Card } from "react-bootstrap";
import {Navigate, useNavigate, useParams} from 'react-router-dom';

import Swal from 'sweetalert2';


import UserContext from "../UserContext"
export default function ProductUpdate() 
{
  // const [image, setImage] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  // const [category, setCategory] = useState("");
  // const [salePercent, setSalePercent] = useState("");
  const [stock, setStock] = useState("");
  const [product, setProduct] = useState({});

  const {user} = useContext(UserContext);
    const navigate = useNavigate();
    const {productId} = useParams();

function ActivateProduct(e)
  {
    e.preventDefault();

    

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`,
    {
        method: "PUT",
        headers:
              {
                "Content-Type" : "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
              },
        body: JSON.stringify(
                    {
                        
                        isActive: true
                     
                    }) 
    }).then(res =>res.json()).then(data =>
        {
            // console.log(data)
            if (data.error)
            {
                Swal.fire({
                    title: `${name}`,
                  icon: "error",
                  text: "Not Updated."
                })
            }
            else
            {
                Swal.fire({
                  title: `${name}`,
                  icon: "success",
                  text: "Product Activated"
                })
                
        navigate(`/products/${productId}`);
            }
        })
    };
    function deActivateProduct(e)
  {
    e.preventDefault();

    

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/deActivate`,
    {
        method: "PUT",
        headers:
              {
                "Content-Type" : "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
              },
        body: JSON.stringify(
                    {
                        
                        isActive: false
                     
                    }) 
    }).then(res =>res.json()).then(data =>
        {
            
            if (data.error)
            {
                Swal.fire({
                    title: `${name}`,
                  icon: "error",
                  text: "Not Updated."
                })
            }
            else
            {
                Swal.fire({
                  title: `${name}`,
                  icon: "success",
                  text: "Product Deactivated"
                })
                
        navigate(`/products/all`);
            }
        })
    };



  function UpdateProduct(e)
  {
    e.preventDefault();

    
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`,
    {
        method: "PUT",
        headers:
              {
                "Content-Type" : "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
              },
        body: JSON.stringify(
                    {
                        
                        name: name,
                      description: description,
                      price: price,
                      
                      stock: stock
                    }) 
    }).then(res =>res.json()).then(data =>
        {
           
            if (data.error)
            {
                Swal.fire({
                    title: `${name}`,
                  icon: "error",
                  text: "Not Updated."
                })
            }
            else
            {
                Swal.fire({
                  title: `${name}`,
                  icon: "success",
                  text: "Product Updated"
                })
                
        navigate(`/products/${productId}`);
            }
        })
    };

    
    useEffect(()=>
    {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
              .then((res) => res.json())
              .then((data) => {

                
                setName(data.name);
                setDescription(data.description);
                setPrice(data.price);
               
                setStock(data.stock);

                setProduct(data);
              });
    },[])

  return (
    <div className="d-flex justify-content-center mt-5 text-center">

      <Card style={{ width: "25rem" }}>
        <Card.Body>
          <Card.Title className="text-center">Update Product</Card.Title>
          <Form onSubmit={(e) => UpdateProduct(e)}>

           

            <Form.Group controlId="formProductName">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product name"
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductDescription">
              <Form.Label>Product Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                placeholder="Enter product description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                required
              />
            </Form.Group>

            

            <Form.Group controlId="formProductStock">
              <Form.Label>Stock</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter stock"
                value={stock}
                onChange={(e) => setStock(e.target.value)}
                required
              />
            </Form.Group>
            
                <Button variant="primary" type="submit" className="text-center justify-content-center mt-5">
                  Update
                </Button>

                </Form>
          
            <div className="d-flex justify-content-center mt-5 text-center">

              <Form onSubmit={(e) => ActivateProduct(e)}>
                <Button variant="primary" type="submit">
                  Activate
                </Button>
              </Form>
              <Form onSubmit={(e) => deActivateProduct(e)}>
                <Button variant="danger" type="submit">
                  Deactivate
                </Button>
              </Form>
            </div>
        </Card.Body>
      </Card>
    </div>
  );
}
