import {useState, useEffect} from 'react';

import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import Activate from './components/Activate';



import Home from './Pages/Home';
import Products from './Pages/Products';
import Register from './Pages/Register';
import Login from './Pages/Login';
import Logout from './Pages/Logout';
import ErrorPage from './Pages/ErrorPage';

import Admin from './Pages/Admin';
import AllProducts from './Pages/AllProducts';
import AddProduct from './Pages/AddProduct';
import UpdateProduct from './Pages/UpdateProduct';
import ProductDetails from './Pages/ProductDetails';
import Cart from './Pages/Cart';

import Orders from './Pages/Orders';

import {Container} from 'react-bootstrap';

import {UserProvider} from './UserContext';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import './App.css'; 

function App() {

  // state hook for the user state that's defined here is for global scope
  // to store the user information and will used for validating if user is logged in on the app or not
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
    // email: localStorage.getItem('email')
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
      setUser({
        id: null,
        isAdmin: null
      })
    }
    })
  }, []); 

  return (
    // <></> fragments - common pattern in React for component to return multiple elements
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          < AppNavbar/>
          <Container>
          <Routes>
            < Route path="/" element={<Home/>}/>
            < Route path="/products/active" element={<Products/>}/>
            < Route path="/products/" element={<Products/>}/>
            < Route path="/products/:productId" element={<ProductView/>}/>
            < Route path="/register" element={<Register/>}/>
            < Route path="/login" element={<Login />}/>
            < Route path="/logout" element={<Logout/>}/>

            < Route path="/admin" element={<Admin/>}/>
            < Route path="/products/all" element={<AllProducts/>}/>
            < Route path="/products/create" element={<AddProduct/>}/>
            < Route path="/products/:productId/update" element={<UpdateProduct/>}/>
            < Route path="/products/:productId/details" element={<ProductDetails/>}/>
            < Route path="/products/:productId/activate" element={<Activate/>}/>

            < Route path="/orders/getAllActive" element={<Orders/>}/>
            < Route path="/cart/show" element={<Cart/>}/>

            < Route path="/*" element={<ErrorPage />} />
          </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;

