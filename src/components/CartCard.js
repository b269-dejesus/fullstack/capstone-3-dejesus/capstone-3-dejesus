// importing from npm packages
import {useContext, useState,useEffect} from "react";
import { Card, Button, ButtonGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { faStar as faStarSolid } from "@fortawesome/free-solid-svg-icons";
import { faStar as faStarRegular } from "@fortawesome/free-solid-svg-icons";
import Swal from 'sweetalert2';

// importing from within the app
import UserContext from "../UserContext"

const CartCard = ({ product }) => 
{
	console.log(product)
	const {
	  productId: {
	  	_id,
	  	image,
	    name,
	    description,
	    price,
	    category,
	    salePercent,
	    finalPrice,
	    stock,
	    buyCount,
	    isActive,
	    createdOn,
	    userFeedback,
	    __v,
	    averageRating,
	  },
	  quantity,
	  subTotal
	} = product;

  const {user} = useContext(UserContext);
  // const navigate = useNavigate();

  const [cartQuantity, setCartQuantity] = useState(quantity);
  const [cartSubTotal, setCartSubTotal] = useState(subTotal);

  const renderStars = () => 
  {
    const fullStars = Math.floor(averageRating);
    const halfStars = Math.ceil(averageRating - fullStars);
    const emptyStars = 5 - fullStars - halfStars;

    const stars = [];

    for (let i = 0; i < fullStars; i++) {
      stars.push(
        <FontAwesomeIcon key={i} icon={faStarSolid} style={{ color: "gold" }} />
      );
    }

    for (let i = 0; i < halfStars; i++) {
      stars.push(
        <FontAwesomeIcon
          key={i + fullStars}
          icon={faStarRegular}
          style={{ color: "gold" }}
        />
      );
    }

    for (let i = 0; i < emptyStars; i++) {
      stars.push(
        <FontAwesomeIcon
          key={i + fullStars + halfStars}
          icon={faStarRegular}
          style={{ color: "gray" }}
        />
      );
    }

    return stars;
  };
    // function for editing quantity
    function handleQuantityChange(value) 
    {
      if (value >= 1 && value <=stock) 
      {
        // console.log(value)
        setCartQuantity(value);
        // console.log(cartQuantity)
        
      }
    };

  	// handling remove from cart
  	function removeFromCart(e)
  	{
  		fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/cart/remove`,
  		{
  	    method: "PUT",
  	    headers:
  	    {
  	      "Content-Type" : "application/json",
  	      Authorization: `Bearer ${localStorage.getItem("token")}`
  	    },
  	    body: JSON.stringify(
  	    {
  	      productId: _id,
  	    })
    	}).then(res =>res.json()).then(data =>
        {
          // console.log(data)
          if (data.error)
          {
            Swal.fire({
              title: `${data.error}`,
                      icon: "error",
                      // text: "Please provide a different email."
            })
          }
          else
          {
            Swal.fire({
                      title: `${data.success}`,
                      icon: "success",
                      // text: "Please proceed to login."
                    })
            
             window.location.reload()
          }
        })
  	}

    // for updating quantity since setCartQuantity is asynchronous
    useEffect(()=>
    {
      fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/cart/add`,
      {
        method: "PUT",
        headers:
        {
          "Content-Type" : "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`
        },
        body: JSON.stringify(
        {
          productId: _id,
          quantity: cartQuantity
        })
      }).then(res =>res.json()).then(data=>
      {
        console.log(data)
        setCartSubTotal(finalPrice*cartQuantity);
      })

      // window.location.reload()
      // navigate("/cart",{state:
      // {
      //   quantity: cartQuantity
      // }})
    },[cartQuantity])

  return (
    <Card style={{ width: "18rem" }} className={isActive ? 'm-2' : 'bg-dark text-light m-2'}>
      <div className="d-flex justify-content-end">
      <Button className="border-0 bg-transparent" onClick={removeFromCart}>
      <FontAwesomeIcon icon={faTrashAlt} className="mt-2 mr-2" size="lg" style={{ color: "red" }}/>
     	</Button>
      </div>
      <Link to={`/products/${_id}`} className="d-flex justify-content-center">
        <Card.Img
          variant="top"
          src={image || "https://via.placeholder.com/200"}
          alt={name}
          className="mt-3"
          style={{ width: "16rem", height: "16rem" }}
        />
      </Link>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text 
          style={{whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis"}}
        >
          {description}
        </Card.Text>
        {salePercent ? (
          <div>
            <span
              style={{
                textDecoration: "line-through",
                marginRight: "10px",
              }}
            >
              ${price}
            </span>
            ${finalPrice} ({salePercent}% off)
          </div>
        ) : (
          <div>${price}</div>
        )}
        {buyCount > 0 && <Card.Text>Products Sold: {buyCount}</Card.Text>}
        {quantity && subTotal && (
          <>
            <ButtonGroup className="mb-3">
              <Button
                variant="secondary"
                onClick={() => handleQuantityChange(cartQuantity - 1)}
              >
                -
              </Button>
              <Button variant="light">{cartQuantity}</Button>
              <Button
                variant="secondary"
                onClick={() => handleQuantityChange(cartQuantity + 1)}
              >
                +
              </Button>
            </ButtonGroup>
            <Card.Text>Subtotal: ${cartSubTotal}</Card.Text>
          </>
        )}
        <Card.Text>{renderStars()}</Card.Text>
      </Card.Body>
    </Card>
  );





};

export default CartCard;

