import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Button, Row, Col, Card, Table } from 'react-bootstrap';

export default function ProductCard({ product }) {

  // Deconstruct the product properties into their own variables
  const { name, description, price, stock, _id } = product;
  const isActive = Boolean(product.isActive);

  return (
    <Card className="mb-3">
      <Card.Body>
        {/*<Card.Title>{name}</Card.Title>*/}
        <Table responsive={true}>
          <thead>
            <tr>
              <th className="text-center">Product Name</th>
              <th className="text-center">Product Description</th>
              <th className="text-center">Product Price</th>
              <th className="text-center">Stocks</th>
              <th className="text-center">Product Status</th>
              <th className="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="text-center">{name}</td>
              <td className="text-center">{description}</td>
              <td className="text-center">{price}</td>
              <td className="text-center">{stock}</td>
              <td className="text-center">{isActive ? "Active" : "Inactive"}</td>

              <td className="text-center">
                <Button className="bg-primary" as={Link} to={`/products/${_id}/update`}>
                  Detail
                </Button>
                
              </td>
            </tr>
          </tbody>
        </Table>
      </Card.Body>
    </Card>
  );
}

