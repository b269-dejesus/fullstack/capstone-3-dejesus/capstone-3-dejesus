// import { useState,useContext,useEffect } from "react";
// import { Form, Button, Card } from "react-bootstrap";
// import {Navigate, useNavigate, useParams} from 'react-router-dom';

// import Swal from 'sweetalert2';


// import UserContext from "../UserContext"
// export default function ProductUpdate() 
// {
//   // const [image, setImage] = useState("");
//   const [name, setName] = useState("");
//   const [description, setDescription] = useState("");
//   const [price, setPrice] = useState("");
//   // const [category, setCategory] = useState("");
//   // const [salePercent, setSalePercent] = useState("");
//   const [stock, setStock] = useState("");
//   const [product, setProduct] = useState({});

//   const {user} = useContext(UserContext);
//     const navigate = useNavigate();
//     const {productId} = useParams();

//   function ActivateProduct(e)
//   {
//     e.preventDefault();

//     //since the function takes the string value of the category and turns it into array, it's important to put conditionals for in case the admin does not update category. Without this, there will be an error due to trying to split an array instead of a string.
//     // let arrayCategory = "";
//     // if(Array.isArray(category))
//     // {
//     //     arrayCategory = category;
//     // }
//     // else
//     // {
//     //     arrayCategory = category.split(/\s*,\s*/);
//     // }

//     // console.log(arrayCategory);

//     fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`,
//     {
//         method: "PUT",
//         headers:
//               {
//                 "Content-Type" : "application/json",
//                 Authorization: `Bearer ${localStorage.getItem("token")}`
//               },
//         body: JSON.stringify(
//                     {
                        
//                         isActive: true
                     
//                     }) 
//     }).then(res =>res.json()).then(data =>
//         {
//             // console.log(data)
//             if (data.error)
//             {
//                 Swal.fire({
//                     title: `${name}`,
//                   icon: "error",
//                   text: "Not Updated."
//                 })
//             }
//             else
//             {
//                 Swal.fire({
//                   title: `${name}`,
//                   icon: "success",
//                   text: "Product Activated"
//                 })
                
//         navigate(`/products/${productId}`);
//             }
//         })
//     };

//     // // Fetching current product details to set getters:
//     // useEffect(()=>
//     // {
//     //     fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
//     //           .then((res) => res.json())
//     //           .then((data) => {

//     //             // console.log(data);

//     //             // if(data.image)
//     //             // {
//     //             //     setImage(data.image);
//     //             // }
//     //             setName(data.name);
//     //             setDescription(data.description);
//     //             setPrice(data.price);
//     //             // setCategory(data.category);
//     //             // setSalePercent(data.salePercent);
//     //             setStock(data.stock);

//     //             setProduct(data);
//     //           });
//     // },[])

//   return (
//     <div className="d-flex justify-content-center mt-5 text-center">
//       <Card style={{ width: "25rem" }}>
//         <Card.Body>
//           <Card.Title className="text-center">Update Product</Card.Title>
//           <Form onSubmit={(e) => UpdateProduct(e)}>

         
//             <Button variant="primary" type="submit" className="text-center justify-content-center mt-5">
//               Update
//             </Button>

//           </Form>
           
//         </Card.Body>
//       </Card>
//     </div>
//   );
// }
