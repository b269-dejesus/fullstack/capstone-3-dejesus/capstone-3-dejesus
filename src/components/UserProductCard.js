  import { useState, useEffect } from 'react';
  import { Link } from 'react-router-dom';
  import { Button, Row, Col, Card } from 'react-bootstrap';

  export default function ProductCard({ product }) {

    // Deconstruct the product properties into their own variables
    const { name, description, price, stock, _id } = product;
    const isActive = Boolean(product.isActive);

    return (
      <Col lg={3} md={4} sm={6} className="mb-4">
        <Card>
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Text>{description}</Card.Text>
            <Card.Text>Price: {price}</Card.Text>
            <Card.Text>Stocks: {stock}</Card.Text>
            <Card.Text>Status: {isActive ? "Active" : "Inactive"}</Card.Text>

            <Button className="bg-primary" as={Link} to={`/products/${_id}/details`}>
              View Product
            </Button>
          </Card.Body>
        </Card>
      </Col>
    );
  }
