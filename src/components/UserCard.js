import {useState, useEffect} from 'react';

import {Link} from 'react-router-dom';

// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
// [S50 ACTIVITY END]


// [S50 ACTIVITY]
export default function UserCard({user}) {

    // Deconstruct the user properties into their own variables
    const { firstName, lastName, email, mobileNo, _id } = user;

 

return (
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{firstName} {lastName}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{email}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>{mobileNo}</Card.Text>
                       {/* <Card.Subtitle>Count: {count} </Card.Subtitle>
                        <Card.Subtitle>Seats: {seats}</Card.Subtitle>*/}
                        {/*<Button variant="primary" onClick={enroll} disabled={seats<=0}>Enroll</Button>*/}
                        <Button className="bg-primary" as={Link} to={`/users/${_id}`}>Details</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row>        
    )
}
// [S50 ACTIVITY END]

